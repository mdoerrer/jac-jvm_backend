package challenge;

import org.springframework.data.repository.CrudRepository;
import challenge.content.DiscardedIdea;

public interface DiscardedIdeaRepository extends CrudRepository<DiscardedIdea, Integer> {

}
