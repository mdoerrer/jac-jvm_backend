package challenge.controller;

import challenge.IdeaRepository;
import challenge.content.Idea;
import challenge.content.IdeaForm;
import challenge.DiscardedIdeaRepository;
import challenge.content.DiscardedIdea;
import challenge.content.DiscardedIdeaForm;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.slf4j.Logger;

@Controller
public class MainController {

  @Autowired
  private IdeaRepository ideaRepository;

  @Autowired
  private DiscardedIdeaRepository discardedIdeaRepository;

  private final Logger logger;
  private List<Idea> ideas;
  private List<DiscardedIdea> discardedIdeas;

  public MainController(Logger logger) {
    this.logger = logger;
    this.ideas = new ArrayList<Idea>();
    this.discardedIdeas = new ArrayList<DiscardedIdea>();
  }

  @RequestMapping(value = "/index")
  public String index() {
    ideaRepository.findAll().forEach(ideas::add);
    discardedIdeaRepository.findAll().forEach(discardedIdeas::add);
    logger.info("Index page accessed");
    return "index";
  }

  @RequestMapping(value = "/ideaList")
  public String ideaList(Model model) {

    model.addAttribute("ideas", ideas);

    logger.info("Page with List of Ideas accessed");
    return "ideaList";
  }

  @RequestMapping(value = "/discardedList")
  public String discardedList(Model model) {

    model.addAttribute("discardedIdeas", discardedIdeas);

    logger.info("Page with List of discarded Ideas accessed");
    return "discardedList";
  }

  @RequestMapping(value = {"/readdIdea"}, method = RequestMethod.GET)
  public String reaAddIdea(Model model) {

    DiscardedIdeaForm idea = new DiscardedIdeaForm();
    model.addAttribute("idea", idea);

    logger.info("Readd Idea Form opened");
    return "readdIdea";
  }

  @RequestMapping(value = {"/readdIdea"}, method = RequestMethod.POST)
  public String saveReeaddedIdea(Model model, @ModelAttribute("idea") DiscardedIdeaForm idea) {
    String name = idea.getName();

    DiscardedIdea old = null;

    if (name != null && name.length() > 0) {
      for (DiscardedIdea foundIdea : discardedIdeas) {
        if (foundIdea.getName().equals(name)) {
          old = foundIdea;
        }
      }
    }
    if (old != null) {
      discardedIdeas.remove(old);
      old.setDiscarded(false);
      Idea newIdea = new Idea(old.getName(), old.getInhalt(), old.isDev(), old.getNameMitarbeiter(),
          old.getPrio(), old.isDiscarded());
      ideas.add(newIdea);
      logger.info("Idea readded");
    }

    return "redirect:/discardedList";
  }

  @RequestMapping(value = {"/addIdea"}, method = RequestMethod.GET)
  public String addIdea(Model model) {

    IdeaForm idea = new IdeaForm();
    model.addAttribute("idea", idea);

    logger.info("Add Idea Form opened");
    return "addIdea";
  }

  @RequestMapping(value = {"/addIdea"}, method = RequestMethod.POST)
  public String saveIdea(Model model, @ModelAttribute("idea") IdeaForm idea) {
    String name = idea.getName();
    String inhalt = idea.getInhalt();
    String dev = idea.getDev();
    String nameMitarbeiter = idea.getNameMitarbeiter();
    int prio = idea.getPrio();
    String discarded = idea.getDiscarded();

    Idea newIdea = null;
    DiscardedIdea newDiscIdea = null;

    if (name != null && name.length() > 0 && inhalt != null && inhalt.length() > 0) {
      if (!discarded.equals("true")) {
        newIdea = new Idea(name, inhalt, Boolean.parseBoolean(dev), nameMitarbeiter, prio,
            Boolean.parseBoolean(discarded));
        ideas.add(newIdea);

        logger.info("Idea added");
      } else if (discarded.equals("true")) {
        newDiscIdea = new DiscardedIdea(name, inhalt, Boolean.parseBoolean(dev), nameMitarbeiter,
            prio, Boolean.parseBoolean(discarded));
        discardedIdeas.add(newDiscIdea);

        logger.info("Idea discarded");
      }
    }
    return "redirect:/ideaList";
  }

  @RequestMapping(value = {"/editIdea"}, method = RequestMethod.GET)
  public String editIdea(Model model) {

    IdeaForm idea = new IdeaForm();
    model.addAttribute("idea", idea);

    logger.info("Edit Idea Form opened");
    return "editIdea";
  }


  @RequestMapping(value = {"/editIdea"}, method = RequestMethod.POST)
  public String saveEditedIdea(Model model, @ModelAttribute("idea") IdeaForm idea) {
    String name = idea.getName();
    String inhalt = idea.getInhalt();
    String dev = idea.getDev();
    String nameMitarbeiter = idea.getNameMitarbeiter();
    int prio = idea.getPrio();
    String discarded = idea.getDiscarded();

    Idea old = null;
    DiscardedIdea oldDisc = null;
    Idea edited = null;
    DiscardedIdea editedDisc = null;

    if (name != null && name.length() > 0 && inhalt != null && inhalt.length() > 0) {
      for (Idea foundIdea : ideas) {
        if (foundIdea.getName().equals(name)) {
          old = foundIdea;
          edited = new Idea(name, inhalt, Boolean.parseBoolean(dev), nameMitarbeiter, prio,
              Boolean.parseBoolean(discarded));
          editedDisc = new DiscardedIdea(name, inhalt, Boolean.parseBoolean(dev), nameMitarbeiter,
              prio, Boolean.parseBoolean(discarded));
          logger.info("Idea edited");
        }
      }
      if (old == null) {
        for (DiscardedIdea foundIdea : discardedIdeas) {
          if (foundIdea.getName().equals(name) && !foundIdea.isDev()) {
            oldDisc = foundIdea;
            edited = new Idea(name, inhalt, Boolean.parseBoolean(dev), nameMitarbeiter, prio,
                Boolean.parseBoolean(discarded));
            editedDisc = new DiscardedIdea(name, inhalt, Boolean.parseBoolean(dev), nameMitarbeiter,
                prio, Boolean.parseBoolean(discarded));
            logger.info("Idea edited");
          }
        }
      }
      if (!discarded.equals("true")) {
        if (oldDisc != null) {
          discardedIdeas.remove(oldDisc);
          ideas.add(edited);
        } else {
          ideas.remove(old);
          ideas.add(edited);
        }
      } else if (discarded.equals("true")) {
        if (oldDisc != null) {
          discardedIdeas.remove(oldDisc);
          discardedIdeas.add(editedDisc);
        } else {
          ideas.remove(old);
          discardedIdeas.add(editedDisc);
        }
      }
    }
    return "redirect:/ideaList";
  }

  @RequestMapping(value = {"/deleteIdea"}, method = RequestMethod.GET)
  public String showDeleteIdea(Model model) {

    IdeaForm idea = new IdeaForm();
    model.addAttribute("idea", idea);

    logger.info("Delete Idea Form opened");
    return "deleteIdea";
  }

  @RequestMapping(value = {"deleteIdea"}, method = RequestMethod.POST)
  public String deleteIdea(Model model, @ModelAttribute("idea") IdeaForm idea) {
    String name = idea.getName();
    Idea removeIt = null;

    if (name != null && name.length() > 0) {
      for (Idea foundIdea : ideas) {
        if (foundIdea.getName().equals(name) && !foundIdea.isDev()) {
          removeIt = foundIdea;
          logger.info("Idea deleted");
        }
      }
    }
    ideas.remove(removeIt);
    return "redirect:/ideaList";
  }

  @RequestMapping(value = "back")
  public String getBack() {
    return "redirect:/ideaList";
  }

  @RequestMapping(value = "load")
  public String load() {
    for (Idea i : ideaRepository.findAll()) {
      ideas.add(i);
    }
    for (DiscardedIdea di : discardedIdeaRepository.findAll()) {
      discardedIdeas.add(di);
    }
    logger.info("Ideas loaded");
    return "redirect:/ideaList";
  }

  @RequestMapping(value = "save")
  public String save() {
    discardedIdeaRepository.deleteAll();
    discardedIdeaRepository.saveAll(discardedIdeas);
    ideaRepository.deleteAll();
    ideaRepository.saveAll(ideas);
    logger.info("Changes saved");
    return "redirect:/ideaList";
  }
}
