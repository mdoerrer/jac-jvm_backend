package challenge.content;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Idea {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  private String name;
  private String inhalt;
  private boolean dev;
  private String nameMitarbeiter;
  private int prio;
  private boolean discarded;

  public Idea() {
  }

  public Idea(String name, String inhalt, boolean dev, String nameMitarbeiter, int prio,
      boolean discarded) {
    this.name = name;
    this.inhalt = inhalt;
    this.dev = dev;
    this.nameMitarbeiter = nameMitarbeiter;
    this.prio = prio;
    this.discarded = discarded;
  }

  public Integer getId() { return id; }

  public void setId(Integer id) { this.id = id; }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getInhalt() {
    return inhalt;
  }

  public void setInhalt(String inhalt) {
    this.inhalt = inhalt;
  }

  public boolean isDev() {
    return dev;
  }

  public void setDev(boolean dev) {
    this.dev = dev;
  }

  public String getNameMitarbeiter() {
    return nameMitarbeiter;
  }

  public void setNameMitarbeiter(String nameMitarbeiter) {
    this.nameMitarbeiter = nameMitarbeiter;
  }

  public int getPrio() {
    return prio;
  }

  public void setPrio(int prio) {
    this.prio = prio;
  }

  public boolean isDiscarded() {
    return discarded;
  }

  public void setDiscarded(boolean discarded) {
    this.discarded = discarded;
  }
}
