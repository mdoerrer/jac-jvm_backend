package challenge.content;

public class DiscardedIdeaForm {

  private String name;
  private String inhalt;
  private String dev;
  private String nameMitarbeiter;
  private int prio;
  private String discarded;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getInhalt() {
    return inhalt;
  }

  public void setInhalt(String inhalt) {
    this.inhalt = inhalt;
  }

  public String getDev() {
    return dev;
  }

  public void setDev(String dev) {
    this.dev = dev;
  }

  public String getNameMitarbeiter() {
    return nameMitarbeiter;
  }

  public void setNameMitarbeiter(String nameMitarbeiter) {
    this.nameMitarbeiter = nameMitarbeiter;
  }

  public int getPrio() {
    return prio;
  }

  public void setPrio(int prio) {
    this.prio = prio;
  }

  public String getDiscarded() {
    return discarded;
  }

  public void setDiscarded(String discarded) {
    this.discarded = discarded;
  }
}
