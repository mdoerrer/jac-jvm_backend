package challenge;

import org.springframework.data.repository.CrudRepository;
import challenge.content.Idea;

public interface IdeaRepository extends CrudRepository<Idea, Integer> {

}
