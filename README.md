# Job Application Challenge - JVM Backend

Hi, you got the link to this repository because you applied for a job at objective partner.

This repository contains a coding challenge for you. The result of this challenge will be reviewed
by the team at objective partner to get an idea of your technical skills and your knowledge about
the basics in software engineering.

## The Epic - Build a Backlog

A customer places an engagement with the following user stories to you.

* As a Team-Member I want to place ideas for further features into a backlog so the idea is
  persisted and can be prioritized
* As a Team-Member I want to be able to edit my idea as long it is not selected for development
* As a Team-Member I want to be able to remove my idea as long it is not selected for development
* As a Team-Member I want to be able to grab an idea to implement it
* As a Product Owner I want to be able to prioritize ideas and select them for development
* As a Product Owner I want to be able to discard an idea with a cause description
* As a Product Owner I want to be able to re-add a discarded idea to the backlog 

## The Challenge

1. Fork this repository into your account, so you are able to work on the challenge.
2. Prioritize the stories above. Start with the (in your opinion) most important story.
3. Use the following tooling
    * Gradle (basic build-script and wrapper already provided)
    * Spring Boot (basic dependencies already provided)
    * Kotlin (build configuration is already provided)
4. Implement a MVP (**M**inimum **V**iable **P**roduct) solution with the following (not necessarily
   complete list) components
    * Server-Component
    * Persistence
    * Web-Client
    * Interface Documentation   
5. Place a Pull-Request at our repository within the Time Box we provided to you.

## The Review

After you placed the pull-request the team will start the review process.
 
Afterwards the team provides its feedback to our talent management and the talent manager, you already
have contact with, will contact you for providing the team decision.
